﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (this.gameObject.CompareTag("Addtime") && other.gameObject.CompareTag("Player"))
        {
            GameManager.timeStart += 5;
            Destroy(this.gameObject);
        }
        if (this.gameObject.CompareTag("Addlife") && other.gameObject.CompareTag("Player"))
        {
            GameManager.lifeplayer += 1;
            if (GameManager.lifeplayer > 3)
            {
                GameManager.lifeplayer = 3;
            }
            Destroy(this.gameObject);
        }
        if (this.gameObject.CompareTag("Addulti") && other.gameObject.CompareTag("Player"))
        {
            GameManager.ulti += 20;
            Destroy(this.gameObject);
        }
    }
}
