﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Utility;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{
    public RigidbodyFirstPersonController player;
    //lifeplayer
    public GameObject heart1, heart2, heart3;
    public static int lifeplayer;
    //gun
    public GameObject CurrentgunisSpray;
    public GameObject CurrentgunisScan;
    private int currrentgun;
    public Image sprayBar;
    public float Currentspray;
    private float Maxspray;
    gun Gun;
    WeaponSwitching currentweapon;
    //score+SceneOver
    public static int ScorePlayer;
    public Text ScoreTxt;
    private bool gameOver = false;
    public static bool gameWinner = false;
    public GameObject gameoverMenu;
    public GameObject gameWinnerMenu;
    public Text ScoreWintxt, ScoreOvertxt;
    public static float totalScore;
    public static float decresScore;
    //time
    public static float timeStart;
    public static bool TimecountCheck;
    public Text TimeTxt;
    //zone
    public static int Zone;
    public Text Zonetxt;
    public Text infectedtxt;
    //ulti
    public static float ulti;
    public Text ultiTxt;

    //
    public static bool GotDamage;
    private float r, g, b, a;
    public Image Overlay;


    void Start()
    {
        //zone
        Time.timeScale = 1f;
        //time
        TimecountCheck = true;
        timeStart = 60;
        TimeTxt.text = timeStart.ToString();
        //score + life
        lifeplayer = 3;
        ScorePlayer = 0;
        totalScore = 0;
        decresScore = 0;
        //gun
        Currentspray = 10;
        currentweapon = FindObjectOfType<WeaponSwitching>();
        Gun = FindObjectOfType<gun>();
        Maxspray = Gun.maxAmmo;
        //ulti
        ulti = 0;

        //.
        GotDamage = false;
        a = 0f;
        r = Overlay.color.r;
        g = Overlay.color.g;
        b = Overlay.color.b;
        a = Overlay.color.a;
    }

    // Update is called once per frame
    void Update()
    {
        if (GotDamage == true ) 
        {
            a += 2f*Time.deltaTime;
        }
        else
        {
            a -= 2f*Time.deltaTime ;
        }
        if(a>=0.5f)
        {
            GotDamage = false;
        }
        a = Mathf.Clamp(a, 0, 1f);
        AdjustColor();
        ultiTxt.text = ulti.ToString("F2") + " %";
        //zone

        switch (Zone)
        {
            case 0:
                Zonetxt.text = "ZoneTown";
                infectedtxt.text = "Infected: " + CountCorona.coronaZonetown.Count.ToString();
                break;
            case 1:
                Zonetxt.text = "ZonePark";
                infectedtxt.text = "Infected: " + CountCorona.coronaZonepark.Count.ToString();
                break;
            case 2:
                Zonetxt.text = "ZoneWalk";
                infectedtxt.text = "Infected: " + CountCorona.coronaZonewalk.Count.ToString();
                break;
        }
        //settext
        totalScore = (ScorePlayer * 100)+decresScore;
        ScoreWintxt.text = "Score: " + Mathf.Round(totalScore).ToString();
        ScoreOvertxt.text = "Score: " + Mathf.Round(totalScore).ToString();
        ScoreTxt.text = "Score: " + totalScore.ToString();


        if (TimecountCheck == true)
        {
            timeStart -= Time.deltaTime;
        }

        if (timeStart > 10)
        {
            TimeTxt.text = Mathf.Round(timeStart).ToString() + " s";
            TimeTxt.color = Color.white;
        }
        else
        {
            TimeTxt.text = timeStart.ToString("F2") + " s";
            TimeTxt.color = Color.red;
        }

        Currentspray = Gun.currentAmmo;
        sprayBar.fillAmount = Currentspray / Maxspray;
        currrentgun = currentweapon.seletedWeapon;
        if (currrentgun == 0)
        {

            CurrentgunisSpray.SetActive(true);
            CurrentgunisScan.SetActive(false);
        }
        else
        {
            CurrentgunisSpray.SetActive(false);
            CurrentgunisScan.SetActive(true);
        }

        if (lifeplayer == 3)
        {
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(true);

        }
        if (lifeplayer == 2)
        {
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(false);

        }
        if (lifeplayer == 1)
        {
            heart1.SetActive(true);
            heart2.SetActive(false);
            heart3.SetActive(false);

        }
        if (lifeplayer == 0)
        {
            heart1.SetActive(false);
            heart2.SetActive(false);
            heart3.SetActive(false);
            totalScore = (ScorePlayer * 100);
            StartCoroutine(Gameover());

        }
        if (timeStart <= 0)
        {
            totalScore = (ScorePlayer * 100);
            StartCoroutine(Gameover());
        }

        if (ScorePlayer >= 71)
        {
            totalScore = (ScorePlayer * 100) + (timeStart * 100);
            StartCoroutine(GameWinner());
        }
        Mathf.Clamp(ulti, 0, 100f);
    }
    public IEnumerator Gameover()
    {
        if (!gameOver)
        {
            yield return  new WaitForSeconds(0.1f);
            Time.timeScale = 0;
            gameOver = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            gameoverMenu.SetActive(true);
            TimeTxt.enabled = false;
            ScoreTxt.enabled = false;
            StopCoroutine(Gameover()); 
            //SceneManager.LoadScene("Gameover");
        }
    }
    public IEnumerator GameWinner()
    {
        if (!gameWinner)
        {
            yield return  new WaitForSeconds(0.1f);
            GotDamage = false;
            Time.timeScale = 0;
            gameWinner = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            TimeTxt.enabled = false;
            ScoreTxt.enabled = false;
            gameWinnerMenu.SetActive(true);
            StopCoroutine(GameWinner());
            //SceneManager.LoadScene("Gameover");
        }
    }
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void BacktoMenu()
    {
        SceneManager.LoadScene("MainMenu");
        SingletonGameApplicationManager.Instance.IsOptionMenuActive = false;
    }

    private void AdjustColor()
    {
        Color c = new Color(r, g, b, a);
        Overlay.color = c;
    }
}
