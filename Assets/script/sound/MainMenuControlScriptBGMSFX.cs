﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class MainMenuControlScriptBGMSFX : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button buttonStart;
    [SerializeField] Button buttonOptions;
    [SerializeField] Button buttonHowToplay;
    [SerializeField] Button buttonCreidt;
    [SerializeField] Button buttonExit;


    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource>();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        SetupButtonsDelegate();
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop();
        audiosourceButtonUI.PlayOneShot(audioclipHoldOver);
    }
    void SetupButtonsDelegate()
    {
        buttonStart.onClick.AddListener(delegate { StartButtonClick(buttonStart); });
        buttonHowToplay.onClick.AddListener(delegate { HowtoplayButtonClick(buttonStart); });
        buttonOptions.onClick.AddListener(delegate { OptionsButtonClick(buttonOptions); });
        buttonCreidt.onClick.AddListener(delegate { CreditButtonClick(buttonOptions); });
        buttonExit.onClick.AddListener(delegate { ExitButtonClick(buttonExit); });


    }
    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("MainGame");
        SingletonGameApplicationManager.Instance.IsOptionMenuActive = true;
    }
    public void OptionsButtonClick(Button button)
    {
        if (!SingletonGameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("OptionScene", LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsOptionMenuActive = true;
            Debug.Log(SingletonGameApplicationManager.Instance.IsOptionMenuActive);
        }
    }
    public void CreditButtonClick(Button button)
    {
            SceneManager.LoadScene("CreditScene", LoadSceneMode.Additive);
    }
    public void HowtoplayButtonClick(Button button)
    {
        SceneManager.LoadScene("HowtoplayScene", LoadSceneMode.Additive);
    }
    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }

}
