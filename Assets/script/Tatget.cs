﻿
using UnityEngine;

public class Tatget : MonoBehaviour
{
    [SerializeField]private Material matBlue;
    [SerializeField]private Material matRed;
    private Material matDefault;
    Renderer mr;
    public float health = 50f;

    void Start()
    {
        mr = GetComponent<Renderer>();

        matDefault = mr.material;
    }
    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }
    void Die()
    {
        Destroy(gameObject);
        if (this.gameObject.CompareTag("NomalHuman"))
            GameManager.lifeplayer -= 1;
    }

    public void TurnRed(){
        Debug.Log("Turn Red");
        mr.material = matRed;
        Invoke("ResetMaterial", 2f);
    }
    public void TurnBlue(){
        Debug.Log("Turn Blue");
        mr.material = matBlue;
        Invoke("ResetMaterial", 2f);
    }

    public void ResetMaterial(){
        mr.material = matDefault;
    }
}
