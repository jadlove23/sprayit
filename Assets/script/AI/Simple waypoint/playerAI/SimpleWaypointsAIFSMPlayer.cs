﻿using System.Collections.Generic;
using KS.Character;
using UnityEngine;
using UnityEngine.AI;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAIPlayer
{
    [RequireComponent(typeof(UnityEngine.Animator))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class SimpleWaypointsAIFSMPlayer : FiniteStateMachine
    {
        public List<Transform> wayPoints;
        
        public GameObject Npc { get; set; }
        public Animator Anim { get; set; }

        [SerializeField] public Transform player;
        public Transform Player
        {
            get { return player; }
        }
        public NavMeshAgent Agent { get; set; }

        public ThirdPersonCharacterKSModified ThirdPersonChar { get; set; }
        public AICharacterControlKSModified AICharControlKsModified { get; set; }
        
        private void Start()
        {
            //The start state
            CurrentState = new SimpleCharacterAIPlayer.Idle1(this);
            
            Npc = GetComponent<Transform>().gameObject;
            
            Anim = GetComponent<Animator>();
            Agent = GetComponent<NavMeshAgent>();
            
            ThirdPersonChar = GetComponent<ThirdPersonCharacterKSModified>();
            AICharControlKsModified = GetComponent<AICharacterControlKSModified>();
        }
    }

    
}

