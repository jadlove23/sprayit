﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAIPlayer
{
    public class Patrol1 : State
    {
        private SimpleWaypointsAIFSMPlayer _simpleWaypointsAifsm;
        private float m_FieldofView;
        private int currentWaypointIdx = -1;
        private bool countScore=false;
        private NavMeshAgent agent;
        private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
        private AICharacterControlKSModified aiCharacterControlKsModified;
        private bool scenClear=false;
        
        public Patrol1(FiniteStateMachine fsm) : base(fsm)
        {
            _simpleWaypointsAifsm = ((SimpleWaypointsAIFSMPlayer) this.FSM);
            
            agent = ((SimpleWaypointsAIFSMPlayer) this.FSM).Agent;
            thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSMPlayer) this.FSM).ThirdPersonChar;
            aiCharacterControlKsModified = ((SimpleWaypointsAIFSMPlayer) this.FSM).AICharControlKsModified;

            
        }

        public override void Enter()
        {
            float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.
            m_FieldofView = 60f;
            // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
            for (int i = 0; i < _simpleWaypointsAifsm.wayPoints.Count; i++)
            {
                GameObject thisWP = _simpleWaypointsAifsm.wayPoints[i].gameObject;
                float distance = Vector3.Distance(_simpleWaypointsAifsm.Npc.transform.position, thisWP.transform.position);
                if(distance < lastDist)
                {
                    currentWaypointIdx = i;
                    lastDist = distance;

                    _simpleWaypointsAifsm.AICharControlKsModified.SetTarget(thisWP.transform);
                }            
            }
            
            //Proceed to the next stage of the FSM
            base.Enter();
        }

        public override void Update()
        {
            Camera.main.fieldOfView = m_FieldofView;
            if (aiCharacterControlKsModified.target != null)
                agent.SetDestination(aiCharacterControlKsModified.target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
            {
                //Move the agent
                
                if (m_FieldofView <= 80f)
                {
                    m_FieldofView++;
                    GameManager.TimecountCheck = false;
                    scenClear = true;
                }
                else
                    thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
                if (!countScore)
                {
                    GameManager.totalScore += GameManager.timeStart;
                    Debug.Log(GameManager.totalScore);
                    countScore = true;
                }

            }
            else
            {
                if (GameManager.ScorePlayer == 5)
                {
                   // Debug.Log("scen1");
                    if (currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 11&&scenClear==true)
                     {
                        
                         if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                        
                    }
                    else
                    {
                            resetTimeScen();
                    }
                    }
                  
                    
                }           
                else if ( GameManager.ScorePlayer == 10)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 10 && scenClear == true)
                    {
                        if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }
                    }
                  
                   
                }
                else if (GameManager.ScorePlayer == 15)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 9 && scenClear == true)
                    {
                    if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }
                    }
                    
                   
                }
                else if (GameManager.ScorePlayer == 20)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 8 && scenClear == true)
                    {if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }


                    }
                    
                    
                }
                else if (GameManager.ScorePlayer == 25)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 7 && scenClear == true)
                    {
                        if (m_FieldofView >= 60f)
                        {
                            m_FieldofView--;
                        }
                        else
                        {
                            resetTimeScen();
                        }
                    }
                    
                }
                else if ( GameManager.ScorePlayer == 30)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 6 && scenClear == true)
                    {
                        if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }

                    }
                    
                }
                else if ( GameManager.ScorePlayer == 35)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 5 && scenClear == true)
                    {
                        if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }

                    }
                    
                }
                else if ( GameManager.ScorePlayer == 41)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 6 && scenClear == true)
                    {if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }

                    }
                    
                }
                else if ( GameManager.ScorePlayer == 51)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 5 && scenClear == true)
                    { if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }

                    }
                   
                }
                else if ( GameManager.ScorePlayer == 56)
                {
                    if(currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 4 && scenClear == true)
                    {
                        if (m_FieldofView >= 60f)
                        {
                            m_FieldofView--;
                        }
                        else
                        {
                            resetTimeScen();
                        }

                    }
                   
                }
                else if (GameManager.ScorePlayer == 61)
                {
                    if (currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 3 && scenClear == true)
                    {if (m_FieldofView >= 60f)
                    {
                        m_FieldofView--;
                    }
                    else
                    {
                            resetTimeScen();
                    }

                    }
                    
                }
                //Set target to the next waypoint
                aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[currentWaypointIdx]);
                agent.SetDestination(aiCharacterControlKsModified.target.position);
                
                //Stop the character movement
                //thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
            }
  
        }
        
        public override void Exit()
        {
            base.Exit();
        }
        private void resetTimeScen()
        {
            countScore = false;
            thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
            GameManager.timeStart = 30;
            GameManager.TimecountCheck = true;
            scenClear = false;

        }
    }
}