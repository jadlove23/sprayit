﻿using UnityEngine;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAIPlayer
{
    public class Idle1 : State
    {
        public Idle1(FiniteStateMachine fsm) : base(fsm)
        {
        }

        public override void Enter()
        {
            //Proceed to the next stage of the FSM state
            StateStage = StateEvent.UPDATE;
        }

        public override void Update()
        {
            if (GameManager.ScorePlayer>=5)
            {
                
                this.FSM.NextState = new SimpleCharacterAIPlayer.Patrol1(this.FSM);
                this.StateStage = StateEvent.EXIT; // The next time 'Process' runs, the EXIT stage will run instead, which will then return the nextState.
            }
        }

        public override void Exit()
        {

        }
    }
}