﻿using KS.Character;
using UnityEngine;
using UnityEngine.AI;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAI
{
    public class Patrol : State
    {
        private SimpleWaypointsAIFSM _simpleWaypointsAifsm;
        private Transform player;
        private int currentWaypointIdx = -1;
        public float lookRadius = 5f;
        private NavMeshAgent agent;
        private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
        private AICharacterControlKSModified aiCharacterControlKsModified;

        public Patrol(FiniteStateMachine fsm) : base(fsm)
        {
            _simpleWaypointsAifsm = ((SimpleWaypointsAIFSM)this.FSM);
            agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
            thirdPersonCharacterKsModified = ((SimpleWaypointsAIFSM)this.FSM).ThirdPersonChar;
            aiCharacterControlKsModified = ((SimpleWaypointsAIFSM)this.FSM).AICharControlKsModified;
            if (agent.CompareTag("Corona"))
            {
                player = ((SimpleWaypointsAIFSM)this.FSM).Player;
            }
        }

        public override void Enter()
        {
            float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.

            // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
            for (int i = 0; i < _simpleWaypointsAifsm.wayPoints.Count; i++)
            {
                GameObject thisWP = _simpleWaypointsAifsm.wayPoints[i].gameObject;
                float distance = Vector3.Distance(_simpleWaypointsAifsm.Npc.transform.position, thisWP.transform.position);
                if (distance < lastDist)
                {
                    currentWaypointIdx = i;
                    lastDist = distance;

                    _simpleWaypointsAifsm.AICharControlKsModified.SetTarget(thisWP.transform);
                }
            }

            //Proceed to the next stage of the FSM
            base.Enter();
        }

        public override void Update()
        {
            if (this.agent.CompareTag("Corona"))
            {
                float distance = Vector3.Distance(player.position, this.agent.transform.position);
                if (distance <= lookRadius)
                {
                   this.FSM.NextState = new SimpleCharacterAI.chaseing(this.FSM);
                   this.StateStage = StateEvent.EXIT;
                }

            }


            if (aiCharacterControlKsModified.target != null)
                agent.SetDestination(aiCharacterControlKsModified.target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
            {
                //Move the agent
                thirdPersonCharacterKsModified.Move(agent.desiredVelocity, false, false);
            }

            else
            {
                //increase waypoint index
                if (currentWaypointIdx >= _simpleWaypointsAifsm.wayPoints.Count - 1)
                    currentWaypointIdx = 0;

                else
                    currentWaypointIdx++;

                //Set target to the next waypoint
                aiCharacterControlKsModified.SetTarget(_simpleWaypointsAifsm.wayPoints[currentWaypointIdx]);
                agent.SetDestination(aiCharacterControlKsModified.target.position);

                //Stop the character movement
                thirdPersonCharacterKsModified.Move(Vector3.zero, false, false);
            }
        }

        public override void Exit()
        {
            base.Exit();
        }


    }

}