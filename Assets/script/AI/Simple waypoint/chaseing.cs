﻿using KS.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace KS.AI.FSM.SampleFSMs.SimpleCharacterAI
{
    public class chaseing : State
    {
        private float timecount;
        private AudioSource soundStep;
        private SimpleWaypointsAIFSM _simpleWaypointsAifsm;
        private Transform player;
        public float lookRadius = 5f;
        private NavMeshAgent agent;
        private ThirdPersonCharacterKSModified thirdPersonCharacterKsModified;
        private AICharacterControlKSModified aiCharacterControlKsModified;
        public chaseing(FiniteStateMachine fsm) : base(fsm)
        {
            agent = ((SimpleWaypointsAIFSM)this.FSM).Agent;
            if (agent.CompareTag("Corona"))
            {
                player = ((SimpleWaypointsAIFSM)this.FSM).Player;
            }
        }

        public override void Enter()
        {
            timecount = 0f;
             //Proceed to the next stage of the FSM state
             StateStage = StateEvent.UPDATE;
            soundStep = agent.GetComponent<AudioSource>();
        }

        public override void Update()
        {
            float distance = Vector3.Distance(player.position, this.agent.transform.position);
            if (distance <= lookRadius)
            {
               
                agent.speed = 1.5f;
                agent.SetDestination(player.transform.position);
                if (timecount <= 0)
                {
                    soundStep.Play();
                    timecount = 0.3f;
                }
                timecount -= Time.deltaTime;

            }
            if(distance > lookRadius)
            {
                agent.speed = 1f;
                soundStep.Stop();
                this.FSM.NextState = new SimpleCharacterAI.Patrol(this.FSM);
                this.StateStage = StateEvent.EXIT;
            }
           
           
        }

        public override void Exit()
        {

        }
    }
}