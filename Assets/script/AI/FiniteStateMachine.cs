﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace KS.AI.FSM
{
public abstract class FiniteStateMachine : MonoBehaviour
{
        public State CurrentState { get; set; }
        public State NextState { get; set; }
        public void ProcessFSM()
        {
            if (CurrentState == null) return;
            switch (CurrentState.StateStage)
            {
                case StateEvent.ENTER:
                    CurrentState.Enter();
                    break;
                case StateEvent.UPDATE:
                    CurrentState.Update();
                    break;
                case StateEvent.EXIT:
                    CurrentState.Exit();
                    CurrentState = NextState;
                    break;


            }

        }
        private void Update()
        {
            ProcessFSM();
        }
    }
}