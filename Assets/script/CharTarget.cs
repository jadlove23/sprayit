﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharTarget : MonoBehaviour
{
   
    [SerializeField] public Zonetype coronazone;
    [SerializeField]public Material matBlue;
    [SerializeField]public Material matRed;
    [SerializeField]public Material matDefault;
    public GameObject DeathSmoke;

    Material[] mats;
    private SkinnedMeshRenderer smr;

    public static bool isScan{get; set;}
    public float health = 10f;
    // Start is called before the first frame update
    void Start()
    {
        smr = GetComponentInChildren<SkinnedMeshRenderer>();
        smr.material = matDefault;
    }

    void Update()
    {
        
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (this.gameObject.CompareTag("Corona")&&collision.gameObject.CompareTag("Player"))
        {
            Hitplayer();
        }
    }
    // Update is called once per frame
    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
            
        }
    }
    public void Die()
    {
        Destroy(gameObject);
        GameObject smoke = Instantiate(DeathSmoke);
        smoke.transform.position = this.transform.position;
        Destroy(smoke,1);
        if (this.gameObject.CompareTag("NomalHuman"))
        {
             GameManager.lifeplayer -= 1;
             GameManager.GotDamage = true;
        }
           
        if (this.gameObject.CompareTag("Corona"))
        {
            GameManager.ScorePlayer += 1;
            GameManager.timeStart += 5;
            GameManager.ulti += 5;
            Debug.Log(GameManager.ScorePlayer);
            switch (coronazone)
            {
                case Zonetype.Town:
                    CountCorona.coronaZonetown.Remove(this.gameObject); break;
                case Zonetype.Park:
                    CountCorona.coronaZonepark.Remove(this.gameObject); break;
                case Zonetype.Shopping:
                    CountCorona.coronaZonewalk.Remove(this.gameObject); break;

            }
               
           
        }
    }

    public void TurnRed(){
      
        smr.material = matRed;
        Invoke("ResetMaterial", 2f);
    }
    public void TurnBlue(){
  
        smr.material = matBlue;
        Invoke("ResetMaterial", 2f);
    }

    public void ResetMaterial(){
        smr.material = matDefault;
    }
    public void Hitplayer()
    {
        GameManager.GotDamage = true;
        GameManager.decresScore -= 100;
        GameManager.ScorePlayer += 1;
        GameManager.lifeplayer -= 1;
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Zonetown")&&this.gameObject.CompareTag("Corona"))
        {

             coronazone = Zonetype.Town;
             CountCorona.coronaZonetown.Add(this.gameObject);
        }
        if (other.gameObject.CompareTag("Zonepark") && this.gameObject.CompareTag("Corona"))
        {
            coronazone = Zonetype.Park;
            CountCorona.coronaZonepark.Add(this.gameObject);
        }

        if (other.gameObject.CompareTag("Zonewalking") && this.gameObject.CompareTag("Corona"))
        {
            coronazone = Zonetype.Shopping;
            CountCorona.coronaZonewalk.Add(this.gameObject);
        }
           
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Zonetown") && this.gameObject.CompareTag("Corona"))
        {
            CountCorona.coronaZonetown.Remove(this.gameObject);
        }

    }
}
