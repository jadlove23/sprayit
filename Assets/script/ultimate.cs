﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ultimate : MonoBehaviour
{
    Collider col;
    private bool usedUlti=false;
    private void Start()
    {
        col = GetComponent<CapsuleCollider>();
    }
    private void Update()

    {
        if(Input.GetKeyDown(KeyCode.Q)&& GameManager.ulti >= 100)
        {
            Useulti();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (usedUlti == true && other.gameObject.tag == "Corona")
        {
            other.gameObject.GetComponent<CharTarget>().TurnRed();

        }
        if (usedUlti == true && other.gameObject.tag == "NomalHuman")
        {
            other.gameObject.GetComponent<CharTarget>().TurnBlue();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if( usedUlti ==true && other.gameObject.tag == "Corona")
        {
            other.gameObject.GetComponent<CharTarget>().TurnRed();
            col.enabled = false;
        }
        if ( usedUlti == true && other.gameObject.tag == "NomalHuman")
        {
            other.gameObject.GetComponent<CharTarget>().TurnBlue();
            col.enabled = false;
        }
    }
    void Useulti()
    {
        usedUlti = true;
        Debug.Log(usedUlti);
        GameManager.ulti = 0;
        col.enabled = true;
    }

}
