﻿
using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    public int seletedWeapon=0;
   
    // Start is called before the first frame update
    void Start()
    {
        SeletedWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        int previousSelectedWeapon= seletedWeapon;
        if(Input.GetKeyDown(KeyCode.Alpha1)){
            seletedWeapon=0;
        }
         if(Input.GetKeyDown(KeyCode.Alpha2)&&transform.childCount>=2){
            seletedWeapon=1;
        }
        if(Input.GetKeyDown(KeyCode.Alpha3)&&transform.childCount>=3){
            seletedWeapon=2;
        }
        if(previousSelectedWeapon != seletedWeapon ){
            SeletedWeapon();
        }
    }
    void SeletedWeapon(){
        int i = 0;
        foreach (Transform weapon in transform){
            if(i==seletedWeapon)
            weapon.gameObject.SetActive(true);
            else
            weapon.gameObject.SetActive(false);
            i++;
        }
    }
}
