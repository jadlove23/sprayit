﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerModified : MonoBehaviour
{
    private AudioSource scanSFX;
    public float range = 100f;
    public Camera fpscam;
    public GameObject impactEffect;
    public float impactForce = 30f;
    public float fireRate = 10f;
    private float nextTimeToFire = 0f;
    public bool CheckScan=false;
    public float TimeCountScan=0f;
    private Transform _selection;
    CharTarget target;

    private void Start()
    {
        scanSFX = gameObject.GetComponent < AudioSource >();
    }
    // Start is called before the first frame update
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Scan();
        }
    }
   public void Scan()
    {
        RaycastHit hit;
        
        if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);
            var selection = hit.transform;
            var selectionRenderer = selection.GetComponent<Renderer>();

            target = hit.transform.GetComponent<CharTarget>();
            if (target != null)
            {
                CheckScan = true;
                TimeCountScan = 2f;
            }
            if (CheckScan == true && hit.collider.gameObject.tag == "Corona")
            {
                scanSFX.Play();
                target.TurnRed();
            }
            if (CheckScan == true && hit.collider.gameObject.tag == "NomalHuman")
            {
                scanSFX.Play();
                target.TurnBlue();
            }

            _selection = selection;
        }

    }
}
