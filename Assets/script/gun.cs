﻿using System.Collections;
using UnityEngine;

public class gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public Camera fpscam;

    public GameObject wetEffect;
    public float impactForce = 30f;

    public float maxAmmo = 10;
    public float currentAmmo;
    public float reloadTime = 1f;
    private bool isReloading = false;

    public float fireRate = 10f;
    private float nextTimeToFire = 0f;
    public Animator animator;
    private AudioSource sparySFX;
    void Start()
    {
        sparySFX = gameObject.GetComponent<AudioSource>();
        if (currentAmmo == -1)
            currentAmmo = maxAmmo;
    }
    void OnEnable()
    {
        isReloading = false;
        animator.SetBool("Reloading", false);
    }
    // Update is called once per frame
    void Update()
    {
        if (isReloading)
        {
            return;
        }
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
        if(Input.GetKey(KeyCode.R) && Time.time >= nextTimeToFire)
        {
            StartCoroutine(Reload());
            return;
        }

    }
    IEnumerator Reload()
    {
        ParticleLauncher.reload = true;
        isReloading = true;
        Debug.Log("REloading");
        animator.SetBool("Reloading", true);
        yield return new WaitForSeconds(reloadTime - .25f);
        animator.SetBool("Reloading", false);
        yield return new WaitForSeconds(.25f);
        currentAmmo = maxAmmo;
        isReloading = false;
        ParticleLauncher.reload = false;
    }
    void Shoot()
    {
        currentAmmo--;
        sparySFX.Play();
        RaycastHit hit;
        if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
        {
            //Debug.Log(hit.transform.name);
            Debug.DrawRay(transform.position, fpscam.transform.forward, Color.green);
            CharTarget target = hit.transform.GetComponent<CharTarget>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            
            GameObject wetDecal = Instantiate(wetEffect, hit.point + new Vector3(0f, 0f, -.02f), Quaternion.LookRotation(-hit.normal));
            Destroy(wetDecal,1f);
        }
    }
}
