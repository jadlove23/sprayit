﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UltimateBar : MonoBehaviour
{
  private Image UltiBar;
  public float CurrentUlti;
  private float MaxUlti = 100f;

  private void Start()
  {
      UltiBar = GetComponent<Image>();
     
  }

  private void Update()
  {
      CurrentUlti = GameManager.ulti;
      UltiBar.fillAmount = CurrentUlti / MaxUlti;
  }
}
